# Start from a base image that includes Node.js
FROM node:14

# Install Python and pip
RUN apt-get update && apt-get install -y python3 python3-pip
RUN pip3 install --upgrade pip

# Install Trufflehog3
RUN pip3 install trufflehog3